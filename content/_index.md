+++
title = "Home"
description = "The general Linux Desktop handbook"
+++

Welcome to the Tux Handbook! This is a beginner and general computer user-friendly introduction to the world of Linux. It covers the basics and tries to help you make choices and even get set up if you choose to do so.

***Disclaimer:** This handbook is written with general computer users in mind and is therefore written with a focus on big picture comprehension. It is not intended to be technically accurate and may simplify some concepts. For an extremely detailed and technically accurate set of Linux documentation, there is always the incredible [ArchWiki](https://wiki.archlinux.org/).*

## What is Linux?

What even is Linux? In a nutshell, it's a collection of operating systems like Windows or macOS.

This short answer doesn't really paint the right picture though, and is entirely unhelpful for picking a distro. I'm afraid no explanation can get it right, so my best advice is to just try it out with a couple of distros. If you'd like to know more before jumping in, however, here's my best attempt at a summary:

**An operating system consists of several major components:**
- A kernel
- A service manager
- A package management system
- A deskop environment (DE for short)

The **kernel** is the thing that does the actual governing of processing, RAM, storage and the like. While it's actually quite important for system stability, power efficiency and performance, most people will never hear about it or care as long as it works. I bring it up because *technically* Linux is a kernel. Windows uses the NT kernel, macOS uses XNU, but ChromeOS and Android use Linux. However most people don't consider ChromeOS or Android to be Linux *operating systems* because the kernel is all they have in common. That said, all Linux *operating systems* use the Linux kernel. It's an annoying nuance that takes a while to get used to. Again, the best way to get it is to try it.

**Service manager**s are hard to explain, but they're very important. Kind of like kernels for higher software, good service managers are extremely important to people in IT. Service managers can also impact system stability and security and such, but the general computer user shouldn't have to care much about it as long as it does it's job. Just know that nearly every modern Linux operating system uses one called `systemd` (short for "system daemon").

A **package management** system is basically how software makes its way onto the computer and is updated. A good package management system can improve system stability can security. It will also make installing stuff faster and more streamlined. In the end, you can usually tell the difference between good and bad package management from whether you dread updates or look forward to them.

**Desktop environment**s are also hard to explain conceptually, though ironically anyone using a computer literally sees them every day. In essence, it's all the stuff you see that the applications sit on. A desktop environment may contain a wallpaper. It could have *panels* with app launchers (like "Launchpad" or the "Start Menu"). Maybe it has a lock screen and a way to control screen brightness and volume.

Unlike the other three major components there is no good or bad desktop environment, just different. Some are really "lightweight", making them good for battery life or older computers. Some have more features or are more polished than others. Some are designed to be more customizable than others. Other than the older computer thing, it really just comes down to preference.

I cannot emphasize this enough: A beginner trying to choose a distro shouldn't consider the other three components much, but should focus on finding a desktop environment they like. It can be the difference between loving and hating Linux. If you don't like your first experience, try a different desktop environment before giving up because it will look and feel completely different. It's common for beginners to hop around distros for a year or so trying to find the one they like best. You'll hear this called "distro hopping."

**In summary**

I've been dropping the word "distro" here and there. That's short for "distribution." A Linux distribution is basically a distribution of the Linux kernel with some combination of a service manager, package manager and desktop environment. (And a bunch of other stuff I've ignored because it's irrelevant in the big picture.) That's right! Unlike Windows or macOS, there's no single Linux operating system. Think of it more like a buffet: you choose your own dish. This is a double edged sword in that you get to choose how you use your computer, but it also forces beginners to make choices they know nothing about.

If you search for "Top Linux distros for beginners" or "How to choose a Linux distro" you'll usually get a bunch of opinions with some overlap. In this handbook I've done my best to present you with the information needed to make a choice instead of handing you my list of preferences.

Total beginners and distro hoppers alike should visit Desktop Environments next, then visit Choosing a Distro after that. Once you've chosen a distro you can try it out without making any changes to your computer by visiting The "Live CD". Good luck!

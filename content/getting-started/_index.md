+++
title = "Getting Started"
description = "How to get started with Linux"
+++

So you've decided to try Linux. How would you even go about doing that? The goal of this section is to help out beginners with installing and setting up Linux.

There's four main steps to this process:

1. [Choosing a distribution](./choosing-a-distro)
1. Preparing the hardware
1. Installing the distribution
1. Installing software

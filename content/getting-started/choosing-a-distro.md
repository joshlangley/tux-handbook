+++
title = "1. Choosing a Distribution"
description = "How to choose a Linux Distribution"
+++

Probably the hardest part for a beginner getting started with Linux is choosing a distribution, or "distro" for short. It's pretty common to search "Best linux distro for beginners" and look at an article like "Top 5 Linux Distros in 2023". The sad truth is that this just doesn't work; there is no "top" linux distro, and though some are more beginner friendly, it rarely has much to do with the distro itself.

A better approach to choosing a distro is to take a second and learn what a distro is. One of the coolest features of Linux is that unlike Windows and macOS, your operating system is made to order. Linux is a buffet, not an assignment. The downside is that beginners must make a choice they know nothing about. The upside is that in the long run, you can use your computer exactly how you like. In fact, this is why distros are often also referred to as "flavors" of Linux. The goal of this article is to help you know a bit about the flavors before making a choice.

Before digging in though, I'd like to stress that choosing a distro is like choosing a flavor at an ice cream parlor. If this time around you choose a flavor you don't like, choose a different one next time. If you want, you can even keep trying different flavors until you find your favorite one. In fact, this pheonomenon of beginners hopping around and trying different flavors of Linux for a while is so common that it has a name: "Distro Hopping".

In short, **if you try a distro and don't like it, it's not that you don't like Linux, you just didn't like the flavor.** Each distro looks and feels very different. Try a few before you decide Linux isn't for you.

## What makes a distro?

A distro comprises of many things, and the differences can sometimes be subtle. Thankfully, there are really only two complete beginners should care about:

1. Desktop Environment
1. System Package Manager

The **desktop envrionment** is essentially the graphical user interface (GUI) you interact with when you use your computer. If you've got a mouse cursor, you've got a desktop envrionment. The desktop environment essentially determines the look and feel of the computer, and consequently how you use it. This makes the desktop environment the biggest preference factor in choosing not just a Linux distro, but any operating system in general. You know how Windows and macOS look and feel totally different? How your phone works totally different from both of those? That's because they have different desktop environments.

**Package Management** are more subtle, but they make a *big* difference too. In a nutshell, package managagement is how you get software (apps, programs, drivers and updates) installed onto your computer. This impacts how easy it is to install apps like Chrome or Discord, and also things like how long system updates take. In the long run, package managagement also impacts how stable your operating system is over time, making it an important factor for choosing a distro (or operating system) too.

On Linux the vast majority of software is installed and updated via the system package manager. There are a great variety of system package managers, and each can be programmed with a different set of packages (called a repository). For the sake of simplicity I am calling combinations of system package managers and repositories a "Package Base".

## The Options

Below is a chart consisting of combinations of desktop environments and package bases, each intersection is a distro. Desktop environments corespond to rows, and package bases correspond to columns. Remember that this chart is by no means exhaustive, but it includes all the major, beginner friendly ones I can think of. You can learn more about each desktop environment and distro in the About Linux section of this site. The corresponding page is also linked in the table below.

**NOTE:** The aforementioned pages are coming soon!

<table>
    <tr>
        <th>Desktop Environment</th>
        <th>Ubuntu</th>
        <th>Fedora</th>
        <th>Manjaro</th>
    </tr>
    <tr>
        <th>GNOME</th>
        <td>Ubuntu</td>
        <td>Fedora</td>
        <td>Manjaro GNOME</td>
    </tr>
    <tr>
        <th>KDE Plasma</th>
        <td>Kubuntu, KDE Neon</td>
        <td>Fedora Spins</td>
        <td>Manjaro Plasma</td>
    </tr>
    <tr>
        <th>XFCE</th>
        <td>Xubuntu, Linux Mint</td>
        <td>Fedora Spins</td>
        <td>Manjaro XFCE</td>
    </tr>
    <tr>
        <th>Cinnamon</th>
        <td>Linux Mint, Ubuntu Cinnamon</td>
        <td>Fedora Spins</td>
        <td>Manjaro Cinnamon</td>
    </tr>
    <tr>
        <th>Pantheon</th>
        <td>Elementary OS</td>
        <td>-</td>
        <td>-</td>
    </tr>
    <tr>
        <th>Budgie</th>
        <td>Ubuntu Budgie</td>
        <td>Fedora Spins</td>
        <td>Manjaro Budgie</td>
    </tr>
    <tr>
        <th>MATE</th>
        <td>Ubuntu MATE, Linux Mint</td>
        <td>Fedora Spins</td>
        <td>Manjaro MATE</td>
    </tr>
</table>



<!--
| Desktop Environment | Debian/Ubuntu               | Fedora          | Manjaro            |
| =================== | =========================== | =============== | ================== |
| GNOME               | Ubuntu                      | Fedora          | Manjaro GNOME      |
| KDE Plasma          | Kubuntu, KDE Neon           | Fedora Spins    | Manjaro Plasma     |
| XFCE                | Xubuntu, Linux Mint         | Fedora Spins    | Manjaro XFCE       |
| Cinnamon            | Linux Mint, Ubuntu Cinnamon | Fedora Spins    | Manjaro Cinnamon   |
| Pantheon            | Elementary OS               | -               | -                  |
| Budgie              | Ubuntu Budgie               | Fedora Spins    | Manjaro Budgie     |
| MATE                | Ubuntu MATE, Linux Mint     | Fedora Spins    | Manjaro MATE       |
-->

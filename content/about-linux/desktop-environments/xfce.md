+++
title = "XFCE"
description = "Lightweight and classic"

[extra]
icon = ""
+++

- **Website:** [https://xfce.org/](https://xfce.org/)
- **Primary graphics toolkit:** Gtk
- **Supports Wayland:** No [(Planned)](https://wiki.xfce.org/releng/wayland_roadmap)
- **App ecosystem:** Base [https://xfce.org/projects#applications](https://xfce.org/projects#applications)
- **ArchWiki page:** [https://wiki.archlinux.org/title/Xfce](https://wiki.archlinux.org/title/Xfce) (very technical)

What's special about XFCE? Nothing. And that's the point. It's just a generic, lightweight Gtk based desktop environment for Linux. It has no frils like animations, but it has a few official plugins for basic things like seeing battery status and connecting to WiFi.

## Distributions

Some common Linux distributions that ship XFCE are:

- Manjaro XFCE
- Xubuntu
- Fedora Spins
- Linux Mint

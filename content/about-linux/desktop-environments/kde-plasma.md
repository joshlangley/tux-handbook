+++
title = "KDE Plasma"
description = "Simple by default, powerful when needed"

[extra]
icon = ""
+++

- **Website:** [https://kde.org/plasma-desktop/](https://kde.org/plasma-desktop/)
- **Primary graphics toolkit:** Qt + KDE Frameworks
- **Supports Wayland:** Yes
- **App ecosystem:** Extensive [https://apps.kde.org/](https://apps.kde.org/)
- **ArchWiki page:** [https://wiki.archlinux.org/title/KDE](https://wiki.archlinux.org/title/KDE) (very technical)

Of all Linux desktops, Plasma has debatably the richest history. Originally the "K Desktop Environment" and eventually morphing into much more than that, KDE has written or inspired some of the most prolific innovations in modern computing. Google Chrome came from KDE's browser base WebKit, and to this day Apple maintains it and uses it in Safari. KDE Connect inspired much of Apple's device integration features and KDE has developed much of the Qt graphics toolkit in general, which thousands of desktop and mobile apps use. Even small things get copied; Microsoft added the dynamically floating task bar to Windows 11 just a few months after KDE added the feature to Plasma.

Plasma is by far the most configurable and customizable desktop environment in the world of Linux, and perhaps even in existence. By default it uses a layout similar to Windows, but can be made to look and feel like macOS just as easily. Plasma uses a completely modular layout of panels and widgets. Like, real widgets, not a renamed version of the MSN News and Interests panel 😜. It is also known for it's incredibly powerful theming engine. KNewStuff also lets you download new widgets and themes without even having to open a web browser. The desktop search system, KRunner, makes even Spotlight look like a toy.

Plasma has too many incredible features to list here, but it comes at the cost of being a bit chaotic. Since KDE mostly consists of people's random contributions over time, it is a massive conglomeration of old and new scraps of code all meshed together. Maintenance can also be pretty spotty on older KDE products. Additionally, the impressive customizability comes at the cost of an incredibly disorganized, inconsistent maze of settings menus.

Qt apps are a first class citizen on Plasma, since it itself is based on Qt. Apps built with the KDE Frameworks will integrate the most seamlessly, but all Qt apps will look and feel right at home. Gtk apps even has as much support as possible right out of the box! The result is that despite *being* an incoherent mix of technologies, actually *using* Plasma is an unexpectedly very clean and seamless experience unmatched by any other desktop.

## Distributions

Some common Linux distributions that ship Plasma are:

- Manjaro Plasma
- Fedora Spins
- Open SUSE Plasma
- KDE Neon
- Kubuntu

KDE also maintains an extensive list at [https://community.kde.org/Distributions](https://community.kde.org/Distributions)

+++
title = "Desktop Environments"
description = "The graphical userspace shell"
+++

The **desktop envrionment** is essentially the graphical user interface (GUI) you interact with when you use your computer. If you've got a mouse cursor, you've got a desktop envrionment. The desktop environment essentially determines the look and feel of the computer, and consequently how you use it. This makes the desktop environment the biggest preference factor in choosing not just a Linux distro, but any operating system in general. You know how Windows and macOS look and feel totally different? How your phone works totally different from both of those? That's because they have different desktop environments.

More technically, the desktop environment is what comes between you and the system. It's what let's you launch Google Chrome and connect to WiFi. The goal of of the desktop environment is to provide some sort of graphical workflow and to provide an alternative to interacting with the computer only from the command line.

## Common Desktop Environments

There are many, many desktop environments you can run on Linux, so this is just a small list of just some of the most common ones. I've grouped them by "modern" and "lightweight". Modern desktop environments will have all the features you've come to expect in the Windows or macOS desktops and then some. Lightweight desktop environments will have less features, but will run much faster on very old computers. Keep in mind the average modern linux desktop environment will be significantly lighter than the Windows and macOS desktops, so if you're a beginner I'd recommend trying a modern desktop environment, and only switching to a lightweight one if it runs slowly.

**Modern**

- GNOME
- KDE Plasma
- Cinnamon
- Pantheon
- Budgie

**Lightweight**

- XFCE
- MATE
- LXQt
- LXDE

## Components

**NOTE: Everything beyond this point is fairly technical!**

- Display Manager (DM)*
- Window Manager (WM)
- Shell
- Panels and Widgets
- Application Ecosystem

**Ha! Indeed! I lumped this in with the DEs for the beginners because what else do you even use this for these days? Come at me high-brow technically-correct snobs!*

In the early days of Linux before desktop environments, managing the X display server was a pain, and so display managers were born. Then window managers were born and people adapted display managers to manage them. Then desktop environments were born and typically wrapped the window managers, so people adapted the display managers to manage the desktop environments. (Yes, I just simplified that history a ton, but the point is there.)

Basically **display manager**s (DMs) have morphed over time into glorified login screens. Back in the 2016-2020 days, it used to be you could choose whatever display manager you liked best and use it with whatever desktop environment you liked. That's still true, except that for desktop environments with SecretService keychains (which is most of them now) you'll have to type in your password again after logging in... unless you use the display manager which automatically unlocks that desktop environment's keychain. Is this dumb? Yes. Ideally it gets fixed at some point with a Freedesktop standard. Is it that important? No. They all basically do the same thing. That's why the table below has a recommended display manager column. Most distros will just include the recommended one, so don't worry about it.

A **window manager** (WM) controls the graphical windows for the applications. In other words, the application doesn't know or care where it's graphical window is, just it's size. The window manager controls where a window is, what size it is and things like minimized, maximized and what "desktop" it's on. Like display managers, you could also choose nearly any combination of window manager and desktop environment you liked, once upon a time. This was nice, because window managers like Compiz had some really fancy animations and workflow improvements you could enable. Modern desktop environments typically provide these workflow improvements and fancy effects live on in some window managers like KWin. There are some legitimate benefits to having custom window managers for each desktop environment, and so that's the model of the modern linux desktop environment.

Desktop **Shell**s are a newer and sometimes controversial concept. Many lighter desktop environments still don't have them. They tend to use a significant amount of memory (from a linux perspective at least; they have nothing on Chrome), but they do unlock a significant amount of nice features. These tend to be a sort of background daemon that does shared processing of things like search indexing (which makes search functions blazingly fast) and syncing data (emails, files, contacts, etc.) with online accounts (like Google Drive or iCloud). Again, you don't really get a choice. Some have them, others don't.

Here's where it gets exciting. **Panels** are typically strips across the side of the screen that have wigets on them. **Docks** are a fancy subset of panels which are typically the smallest size possible, and they may or may not hide themselves to get out of the way.

**Widgets** are things that go on panels, although they may also be able to go on the desktop depending on the desktop environment. To be clear, I'm talking about real desktop widgets. To learn more about the ~~MSN News and Interests~~ *ahem* "widgets" panel in Windows 11 that opens in your face with murder stories in the middle of important meetings, see [here](https://support.microsoft.com/en-us/windows/stay-up-to-date-with-news-and-interests-a39baa08-7488-4169-9ed8-577238f46f8f).

Widgets consist of things like:
- **App Launchers** - to launch apps (like Launchpad on macOS or the Start Menu on Windows)
- **Task managers** - where you can pin your commonly used apps, launch them, and switch between them
- **Pagers** - which let you switch between virtual desktops
- **Trash shortcuts**
- **System trays** - that show icons for volume controls, battery status, network and bluetooth connections, and maybe icons for apps running in the background
- **Clocks** - digital clocks, analog clocks, artistic clocks, binary clocks, fuzzy clocks, you name it!
- **Color pickers**
- **Pictures** - especially on the desktop
- **System statistics graphs** - display CPU/Memory/Network usage, temperatures, etc.
- **Weather and news summaries**
- **And much, much more** - Linux people are incredibly creative and come up with all kinds of useful things to put on their panels and desktops

Most desktop environments let you customize your desktop layout (combination and position of panels, docks and widgets) to some degree. Some are built around customization and others are more limited. A notable exception is GNOME, which is customizable only by means of extensions, much like a modern web browser.

**Application Ecosystems** are more or less a thing of yesterday, depending on how you look at it. In the traditional sense, desktop environment applications were things like a file manager, settings app, terminal and calculator; little utilities that went with that desktop environment. This really hasn't changed much.

What's new is that some desktop environments are starting to see lots of apps coming out for lots of specific tasks, which are designed with that desktop environment in mind. Music players and video editors, messaging apps and email clients, even things like IDEs and game launchers. It's stunning to see the breadth of apps people are coming out with for these ecosystems. It's hard to know who started it. KDE's got ecosystem apps out there from nearly a decade ago, and one could argue GNOME's been at it for about as long. ElementaryOS was probably the first to start polishing things up and unifying them in a sort of standard. Either way, it's a nice thing to use a desktop environment with a full ecosystem.

## Common Desktop Environments



| Desktop  | Developer    | Type        | WM     | Common DM | Common Distros                       |
| ======== | ============ | =========== | ====== | ========= | ==================================== |
| GNOME    | GNOME        | Featureful  | Mutter | GDM       | Fedora, Manjaro, Ubuntu              |
| Plasma   | KDE          | Featureful  | KWin   | SDDM      | KDE Neon, Manjaro, Kubuntu, Steam OS |
| Pantheon | elementaryOS | Featureful  |        | LightDM   | elementaryOS                         |
| Budgie   | Budie        | Featureful  |        | GDM       | Ubuntu Budgie, Manjaro               |
| Cinnamon | Linux Mint   | Simple      |        | LightDM   | Linux Mint, Manjaro, Ubuntu Cinnamon |
| XFCE     | XFCE         | Lightweight |        | LightDM   | Xubuntu, Linux Mint, Manjaro         |
| MATE     | MATE         | Lightweight |        | LightDM   | Ubuntu MATE, Linux Mint, Manjaro     |

The common distros column is not an exhaustive list by any means. It simply lists some of the most popular distros which provide versions with that desktop environment.


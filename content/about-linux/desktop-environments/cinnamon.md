+++
title = "Cinnamon"
description = "Aero's back baby!"

[extra]
icon = ""
+++

- **Website:** None
- **Primary graphics toolkit:** Gtk
- **Supports Wayland:** No (Experimental session available)
- **App ecosystem:** Base
- **ArchWiki page:** [https://wiki.archlinux.org/title/Cinnamon](https://wiki.archlinux.org/title/Cinnamon) (very technical)

Do you ever miss the Windows 7 desktop with it's slick Aero theme? How it just stayed out of your face and let you get stuff done? Do you miss widgets and a simple theming system?

Cinnamon may not have Aero (at least not by default), but it does feel uncannilly like the modern reincarnation of the Windows 7 desktop. It's a no-nonsense, robust desktop that's just featureful enough to pass as a modern desktop environment, but also almost light enough to pass as lightweight desktop. It's customizable enough, with a competent suite of "applets" and themes available right from the settings menus (like KDE). There's not really any flashy new features, but it's got a cozy sort of rock-solidness that just lets you get stuff done. It's native themes (Mint-X and Mint-Y) are classy, it's file manager (Nemo) is practical and more useful than GNOME's.

A hard fork of GNOME 3, the Cinnamon desktop is developed by the Linux Mint team. Wayland support isn't here yet but is in progress. Like most Gtk desktops, Qt isn't well supported but does work fine. It has a file manager and some settings apps, so no real app ecosystem of it's own to speak of, but like XFCE any classic Gtk 3 app will look and feel right at home.

## Distributions

Some common Linux distributions that ship Cinnamon are:

- Linux Mint
- Manjaro Cinnamon
- Fedora Cinnamon
- Ubuntu Cinnamon

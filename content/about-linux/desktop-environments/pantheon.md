+++
title = "Pantheon"
description = "The simple power of the elementary"

[extra]
icon = ""
+++

- **Website:** None
- **Primary graphics toolkit:** Gtk + Graphite
- **Supports Wayland:** No (in progress)
- **App ecosystem:** Extensive [https://appcenter.elementary.io/](https://appcenter.elementary.io/)
- **ArchWiki page:** [https://wiki.archlinux.org/title/Pantheon](https://wiki.archlinux.org/title/Pantheon) (very technical)

Pantheon, the native desktop environment to Elementary OS, can be summarized in a word: elegance. It has an extremely clean, well integrated feel. It's very well known for it's extensive, well maintained app ecosystem.

## Distributions

Some common Linux distributions that ship Pantheon are:

- Elementary OS

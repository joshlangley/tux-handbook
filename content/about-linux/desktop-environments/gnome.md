+++
title = "GNOME"
description = "A unique take on workflow"

[extra]
icon = ""
+++

- **Website:** [https://www.gnome.org](https://www.gnome.org/)
- **Primary graphics toolkit:** Gtk / libadwaita
- **Supports Wayland:** Yes
- **App ecosystem:** Extensive [https://apps.gnome.org/](https://apps.gnome.org/)
- **ArchWiki page:** [https://wiki.archlinux.org/title/GNOME](https://wiki.archlinux.org/title/GNOME) (very technical)

The GNOME desktop environment takes a unique and innovative approach to computing workflow. It's clean, well-integrated interface provides a simple and intuitive experience for all computer users. It's the most popular of modern Linux desktops, but it's also probably the most heavy. The application ecosystem is rich, and it's online account integration is unparalelled.

What sets this desktop environment apart is the unique, seamless workflow. It has a focus on workspaces, which are basically virtual desktops. Overview mode reveals a dock, app launcher, and an overview of active workspaces. Managing open apps was never so easy and seamless.

Since version 40, GNOME has started to fill the role of the "Apple of the Linux world". It has a strong focus on brand, identity, consistency of experience, simplicity and stability, but at the expense of customization. System-wide themeing is difficult discouraged, though possible. Extensions do exist and are required to get things like a system tray and desktop icons, but if you have glitches the first thing you'll be asked to do is disable all extensions.

GNOME is fundumentally a Gtk-based desktop environment, but unlike XFCE, Cinnamon and other Gtk desktops it integrates best with apps using it's own libadwaita framework, which is a derivative of Gtk 4. Like most Gtk apps, Qt is not well supported, so stick with Gtk-based apps when possible.

## Distributions

Some common Linux distributions that ship GNOME are:

- Fedora
- Ubuntu
- Manjaro

GNOME also maintains a page about this at [https://www.gnome.org/getting-gnome/](https://www.gnome.org/getting-gnome/)
